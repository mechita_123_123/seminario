<?php

// Iniciamos la conexión a la Base de Datos
$id = $_GET['varid'];

$con = mysqli_connect('localhost','root','','registros');
// Le pedimos que imprima los caracteres especiales
$con->query("SET NAMES 'utf8'");
                     
// Si hay error que nos arreoje un mensaje                     
if ($con->connect_error) {
    die('Error en la Conexión a la Base de Datos : ('. $con->connect_errno .') '. $con->connect_error);
}

// Seleccionamos los registros
$results = $con->query("select *  from v_listainmueble WHERE id_inmueble=".$id);

// Creamos el array postres
$rows['lista_inmuebledet'] = array();

// Recorremos los registros de la Base de Datos para mostrarlos
while($r = $results->fetch_object()) {
    array_push($rows['lista_inmuebledet'], $r);
}

// Con solo json_encode imprimimos los registros, pero le agregamos 128 que es el valor numérico de la extensión JSON_PRETTY_PRINT para mostrar el array mas ordenado en pantalla
$mysql_json = json_encode($rows);

// Con las etiquetas <pre></pre> damos saltos de linea a nuestro array
print($mysql_json);

//$file_mapa = 'gmapas.json';
//file_put_contents($file_mapa, $mysql_json);

?>