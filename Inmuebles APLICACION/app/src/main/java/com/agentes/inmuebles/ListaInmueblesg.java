package com.agentes.inmuebles;

/**
 * Created by Dennis on 21/12/2014.
 */
public class ListaInmueblesg {
    private String id;
    private String nombretipo;
    private String nombre;
    private String descripcion;

    public ListaInmueblesg(String id, String nombretipo, String nombre, String descripcion) {
        this.id = id;
        this.nombretipo = nombretipo;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public String getID() {
        return id;
    }
    public String getNombretipo() {
        return nombretipo;
    }
    public String getNombre() {
        return nombre;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setID(String id) {
        this.id = id;
    }
    public void setNomtipo(String nomtipo) {
        this.nombretipo = nomtipo;
    }
    public void setNom(String nom) {
        this.nombre = nom;
    }
    public void setDescrip(String descrip) {
        this.descripcion = descrip;
    }
}
