package com.agentes.inmuebles;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import android.util.Log;
import android.view.View;
import android.app.ProgressDialog;
import com.squareup.okhttp.OkHttpClient;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;
/**
 * An activity that displays a Google map with a marker (pin) to indicate a particular location.
 */
public class ActividadMapas extends AppCompatActivity
        implements OnMapReadyCallback {

    public View VistaGenV = null;
    private ProgressDialog pDialog;
    //public ListView listview;
    public OkHttpClient cliente = new OkHttpClient();

    //private static String http_s = "http://10.0.3.2/inmueble/mapasjson.php?varidm=2";
    private static String http_s;

    private static final String TAG_TABLA   = "mapas_inmueble";
    private static final String TAG_ID      = "id_ubicacioninmueble";
    private static final String TAG_ZONA  = "zona";
    private static final String TAG_LAT  = "latitud";
    private static final String TAG_LONG  = "longitud";

    public SharedPreferences editorAlert;
    public SharedPreferences.Editor editor;

    public JSONArray jsondata = null;
    public String agenda;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve the content view that renders the map.
        setContentView(R.layout.actividadmapas);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Bundle args = getIntent().getExtras();
        if (args != null && args.containsKey("MId")) {
            agenda = args.getString("MId");

            //url = "http://10.0.3.2/inmueble/inmueble_masterdet.php?varid=" + agenda;
            http_s = "http://10.0.3.2/inmuebles/public/inmueblemapa?varmap=" + agenda;
            //Log.v("Text Data-->", url);
        }

        // Ge t the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_container);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map when it's available.
     * The API invokes this callback when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user receives a prompt to install
     * Play services inside the SupportMapFragment. The API invokes this method after the user has
     * installed Google Play services and returned to the app.
     */
    public String runjson(String urlj) throws Exception {
        Request request = new Request.Builder()
                .url(urlj)
                .build();
        if(request != null){
            Response response = cliente.newCall(request).execute();
            if (response.isSuccessful()) {
                return response.body().string();
            }else {
                return null;
            }
        }else{
            return null;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        //-19.579636, -65.754958
        String jsonStr = null;
        try {
            jsonStr = runjson(http_s);
            //Log.v("Text Data-->", jsonStr);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);

                // Getting JSON Array node
                jsondata = jsonObj.getJSONArray(TAG_TABLA);

                Random randomGenerator = new Random();
                boolean randomBool;
                for (int i = 0; i < jsondata.length(); i++) {
                    JSONObject c = jsondata.getJSONObject(i);

                    double st_lat = c.getDouble(TAG_LAT);
                    double st_lon = c.getDouble(TAG_LONG);
                   // String st_zona = c.getString(TAG_ZONA);

                    Log.d("Latitud->", String.valueOf(st_lat));
                    Log.d("Longitud->", String.valueOf(st_lon));

                    LatLng sydney = new LatLng(st_lat, st_lon);
                    googleMap.addMarker(new MarkerOptions().position(sydney)
                            .title(c.getString(TAG_ZONA)));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("ServiceHandler", "Couldn't get any data from the url");
        }
        //return null;


//           LatLng sydney = new LatLng(-19.579636, -65.754958);
//           LatLng sydney2 = new LatLng(-19.579235, -65.753030);
//           LatLng sydney3 = new LatLng(-19.580964, -65.755938);
//           LatLng sydney4 = new LatLng(-19.582359, -65.753513);
//           googleMap.addMarker(new MarkerOptions().position(sydney)
//           .title("Terreno A"));
//           googleMap.addMarker(new MarkerOptions().position(sydney2)
//            .title("Terreno B sidney t wo"));
//           googleMap.addMarker(new MarkerOptions().position(sydney3)
//                   .title("Terreno C"));
//           googleMap.addMarker(new MarkerOptions().position(sydney4)
//              .title("Terreno D"));
//           googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney3));
//           googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney2, 16));


    }
}