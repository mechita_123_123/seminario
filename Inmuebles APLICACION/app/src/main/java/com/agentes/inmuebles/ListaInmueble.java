package com.agentes.inmuebles;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.util.Log;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

//import com.google.android.gms.common.api.Response;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Usuario on 29/11/2017.
 */
public class ListaInmueble extends Fragment{
    public View VistaGenV = null;
    private ProgressDialog pDialog;
    //public ListView listview;
    public OkHttpClient cliente = new OkHttpClient();

    private static String url = "http://10.0.3.2/inmueble/inmueble_master.php";
    private static final String TAG_TABLA = "lista_inmueble";
    private static final String TAG_ID = "id_inmueble";
    private static final String TAG_NOMBRE_TIPO = "nombre_tipo_inmueble";
    private static final String TAG_NOMBRE = "nombre_inmueble";
    private static final String TAG_DESCRIPCION = "dimensiones_inmueble";
    public JSONArray js_registro = null;

    public JSONArray jsondata = null;
    //public JSONArray jsondatac = null;
    private RecyclerView.Adapter adaptador;
    private RecyclerView.LayoutManager lManager;
    private List<ListaInmueblesg> rowItems;
    private RecyclerView mRecyclerView;

    private ArrayList<String> stringArray;
    public static ArrayList<String> stringArraySpinner;

    private Boolean dExiste; //EXISTEN DATOS
    private Boolean cExiste; //EXSTEN CONEXION
    private String cCategoria = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.lista_inmueble, container, false);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        VistaGenV = rootView;

        mRecyclerView = (RecyclerView) VistaGenV.findViewById(R.id.inmuebContainer);

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever
                        final String ATRID = ((TextView) view.findViewById(R.id.textVid)).getText().toString();
                        final Bundle bundle = new Bundle();
                        bundle.putString("atrid", ATRID);

                        Intent AAgenda = new Intent(getActivity(), ListaInmuebleDetalle.class);
                        AAgenda.putExtras(bundle);
                        startActivity(AAgenda);

//                        Log.d("Cliqueado: ", "> " + ATRID);
                    }
                })
        );
        new GetAgendaG().execute();
        return rootView;
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            *//*case android.R.id.home:
                this.finish();// app icon in action bar clicked; go home
                return true;*//*
            *//*case R.id.action_reload:
                new GetAgendaG().execute();//new GetAgendaG().onPreExecute(); //recarga nuevamente la data
                return true;*//*
            case R.id.action_filtro_art:
                ////DIALGO DE FILTRO Y BUSQUEDA////
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                // Get the layout inflater
                LayoutInflater inflater = getActivity().getLayoutInflater();
                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                View prp = inflater.inflate(R.layout.frag_listainmuebles, null);
                final Spinner mySpinner = (Spinner) prp.findViewById(R.id.sCategoriasArtistas);

                ArrayList<String> a = stringArraySpinner;
                //Log.d("Cadeba ->", String.valueOf(a));
                mySpinner.setAdapter(new ArrayAdapter<String>(this.getActivity(),
                        android.R.layout.simple_spinner_dropdown_item,
                        a));
                //Log.d("Cadeba ->", String.valueOf(a));
                builder.setView(prp)
                        // Add action buttons
                        .setPositiveButton(R.string.filtro_ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                // sign in the user ...
                                int position = mySpinner.getSelectedItemPosition();
                                //Log.d("ItemSEL ->", String.valueOf(position));
                                url = "http://www.culturapotosi.com.bo/json/artistas/c/" + position; //position;
                                //Log.d("URLSEL ->", url);
                                cCategoria = mySpinner.getSelectedItem().toString();
                                dialog.dismiss();
                                ReLoadAgenda();
                            }
                        })
                        .setNegativeButton(R.string.filtro_cancelar, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }*/
    //OBTENEMOS LOS DATOS DEL STRING
    public String runjson(String urlj) throws Exception {
        Request request = new Request.Builder()
                .url(urlj)
                .build();
        if(request != null){
            Response response = cliente.newCall(request).execute();
            if (response.isSuccessful()) {
                return response.body().string();
            }else {
                return null;
            }
        }else{
            return null;
        }
    }
    ///LECUTRA ASINCRONA DE EL JSON
    private class GetAgendaG extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Cargando...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            String jsonStr = null;
            cExiste = true;
            dExiste = true;
            try {
                jsonStr = runjson(url);
                //Log.v("Text Data-->", jsonStr);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (jsonStr == null) {
                cExiste = false;
            }else if(jsonStr.length() == 0){
                dExiste = false;
            }
            if(dExiste && cExiste){
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    jsondata = jsonObj.getJSONArray(TAG_TABLA);
                    rowItems = new ArrayList<ListaInmueblesg>();

                    for (int i = 0; i < jsondata.length(); i++) {
                        JSONObject c = jsondata.getJSONObject(i); //OBJETO
                        String JSID = c.getString(TAG_ID);
                        String JSNombreTipo = c.getString(TAG_NOMBRE_TIPO);
                        String JSNombre = c.getString(TAG_NOMBRE);
                        String JSDescripcion = c.getString(TAG_DESCRIPCION);

                        ListaInmueblesg items = new ListaInmueblesg(JSID, JSNombreTipo, JSNombre, JSDescripcion);
                        rowItems.add(items);
                        //}
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            //LECTURA DE JSON PARA SPINNER
            //CargaSpinner();
            //
            //
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            if(!cExiste) {
                Toast.makeText(getActivity(), "La aplicacion no puede conectarse al servidor\nPor favor revise su conexion a internet.", Toast.LENGTH_LONG).show();
            }else{
                if(!rowItems.isEmpty()) {
                    lManager = new LinearLayoutManager(getContext());
                    mRecyclerView.setLayoutManager(lManager);
                    //
                    adaptador = new ListaInmuebleAdap(getActivity(), rowItems);
                    mRecyclerView.setAdapter(adaptador);
                    //mRecyclerView = (RecyclerView) VistaGenV.findViewById(R.id.calglista);
                }else{
                    Toast.makeText(getActivity(), "Lo sentimos la lista esta vacia.", Toast.LENGTH_SHORT).show();
                }
            }
            if(cExiste && !dExiste) {
                Toast.makeText(getActivity(), "La lista de la Categoria " + cCategoria +" esta vacia\nPor favor elija otra categoria.", Toast.LENGTH_LONG).show();
            }
//            mySpinner

            //
        }

    }
    public void ReLoadAgenda(){
        new GetAgendaG().execute();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

}
