package com.agentes.inmuebles;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;

public class ActividadReginm extends AppCompatActivity {
    EditText txtData;
    Button btnWriteSDFile;
    OkHttpClient client = new OkHttpClient();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividadreginm); //activity_main2 es el XML donde estara los datos de formulario
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtData = (EditText) findViewById(R.id.editTextNombre); //eSTE EL Edit Text donde se escribe
        txtData.setHint("Escribe tus datos..."); //esto es como un place holder

        btnWriteSDFile = (Button) findViewById(R.id.btn_aceptarri);
        btnWriteSDFile.setOnClickListener(new AdapterView.OnClickListener() {

            public void onClick(View v) {
                // write on SD card file data in the text box

                //public void run() throws Exception {
                RequestBody formBody = new FormEncodingBuilder()
                        .add("dato", txtData.getText().toString()) /// Aqui se converte la cadena el  TEXT edit | dato es el nombre de parametro a enviar
                        .add("tipo", "guardar") //tipo es otro parametro, asi puedes ir agregando. con add los parametros necearios.
                        .build();
                Request request = new Request.Builder()
                        .url("http://10.0.3.2/inmueble/reg_inmueble.php") /// URL DONDE ENVIAR LA IP LOCAL la consgui desde linea de comandos con ipconfig
                        .post(formBody)
                        .build();
                try {
                    Response response = client.newCall(request).execute(); // esto devuelve si la conexion fue correcta a la IP, no te avisa si ocurre un error de codigo PHP, solo si se conecto.
                    Toast.makeText(getBaseContext(), response.toString(),
                            Toast.LENGTH_SHORT).show();
                    // Do something with the response.
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }// onClick
        }); // btnWriteSDFile

    }

}