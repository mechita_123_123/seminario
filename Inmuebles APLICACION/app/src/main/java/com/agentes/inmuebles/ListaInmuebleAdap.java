package com.agentes.inmuebles;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

//import com.bumptech.glide.Glide;
// arriba

import java.util.List;

//import jp.wasabeef.glide.transformations.CropSquareTransformation;
//arriba

public class ListaInmuebleAdap extends RecyclerView.Adapter<ListaInmuebleAdap.ListaInmuebleViewHolder> {
    private Context context;
    private List<ListaInmueblesg> items;
    public static class ListaInmuebleViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public Context context;
        public TextView txtNombretipo;
        public TextView txtNombre;
        public TextView txtID;
        public TextView txtDescripcion;
    public ListaInmuebleViewHolder(View v) {
            super(v);
            txtNombretipo = (TextView) v.findViewById(R.id.textVnombretipo);
            txtNombre = (TextView) v.findViewById(R.id.textVnombre);
            txtDescripcion = (TextView) v.findViewById(R.id.textVdescripcion);
            txtID = (TextView) v.findViewById(R.id.textVid);
        }
    }
    public ListaInmuebleAdap(Context context, List<ListaInmueblesg> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ListaInmuebleViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lista_inmueble_lista, viewGroup, false);
        return new ListaInmuebleViewHolder(v);
    }
    @Override
    public void onBindViewHolder(ListaInmuebleViewHolder viewHolder, int i) {
        viewHolder.txtNombretipo.setText(items.get(i).getNombretipo());
        viewHolder.txtNombre.setText(items.get(i).getNombre());
        viewHolder.txtID.setText(items.get(i).getID());
        viewHolder.txtDescripcion.setText(items.get(i).getDescripcion());
        //Glide.with(context).load(items.get(i).getFoto()).bitmapTransform(new CropSquareTransformation(context)).into(viewHolder.imagen);
        //arriba

        //centerCrop().bitmapTransform(new CropCircleTransformation(context))

    }
}
