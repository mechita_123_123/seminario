package com.agentes.inmuebles;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.bumptech.glide.Glide;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ListaInmuebleDetalle extends AppCompatActivity {
    private static String url = "";
    private ProgressDialog pDialog;

    private static final String TAG_TABLA = "lista_inmuebledet";
    private static final String TAG_ID = "id_inmueble";
    private static final String TAG_NOMBRE = "nombre_inmueble";
    private static final String TAG_DIM = "dimensiones_inmueble";
    private static final String TAG_UBICA = "ubicacion_inmueble";
    private static final String TAG_GESTION = "gestionconstruccion_inmueble";
    private static final String TAG_CANTIDAD = "cantidadhabitaciones_inmueble";
    private static final String TAG_COSTO = "costo_inmueble";


    public OkHttpClient client = new OkHttpClient();
    public JSONArray jsondata = null;
    public String agenda = "1";
    public String aglugar = "1";
    public String jsonStrSlide = null;

    //
    public String MInmuebleID = "";
    public String MNombre = "";
    public String MDimension = "";
    public String MUbica = "";
    public String MGestion = "";
    public String MCantiHab = "";
    public String MCosto = "";
    //
    public Boolean abrir = true;

    //
    public ImageButton btnMail;
    public ImageButton btnWeb;
    public ImageButton btnCall;
    public ImageButton btnMat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_inmueble_detalle);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if (chkInternet()) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarAr);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    /*        ActionBar ab = getSupportActionBar(); //getActionBar();
            assert ab != null;
            ab.setDisplayHomeAsUpEnabled(true);*/

            Bundle args = getIntent().getExtras();

            if (args != null) {
                for (String key : args.keySet()) {
                    Object value = args.get(key);
                    Log.d("FB->", "Key: " + key + " Value: " + value.toString());
                }
            }
            if (args != null && args.containsKey("atrid")) {
                agenda = args.getString("atrid");
                if (args.getString("dealerta") == null) {
                    abrir = false;
                }

                url = "http://10.0.3.2/inmueble/inmueble_masterdet.php?varid=" + agenda;
                //Log.v("Text Data-->", url);
            }

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabAr);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("MId", agenda);
                    ///
                    /*bundle.putString("item", item);
                    bundle.putString("cate", cate);*/
                    /**
                     * Variables de latitud longitud
                     */

                    //ABRIR LA SIGUIENTE ACTIVITY
                    Intent ARecinto = new Intent(ListaInmuebleDetalle.this, ActividadMapas.class);
                    ARecinto.putExtras(bundle);
                    startActivity(ARecinto);
                }
            });
            //
            //

/*            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabAr);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String urls = MInmueble + "\nhttp://10.0.3.2/inmueble/inmueble_masterdet.php?varid=" + agenda;
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, urls);
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                }
            });

            btnWeb = (ImageButton) findViewById(R.id.imgBtnWebAr);
            btnWeb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(MWeb));
                    startActivity(i);
                }
            });

            btnMat = (ImageButton) findViewById(R.id.imgBtnMat);
            btnMat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Bundle bundle = new Bundle();
                    bundle.putString("atrid", MInmID);
                    bundle.putString("atrtext", MInmueble);
                    Intent AAgenda = new Intent(ListaInmuebleDetalle.this, Material.class);
                    AAgenda.putExtras(bundle);
                    startActivity(AAgenda);
                }
            });

            btnMail = (ImageButton) findViewById(R.id.imgBtnEmailAr);
            btnMail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent mailer = new Intent(Intent.ACTION_SEND);
                    mailer.setType("text/plain");
                    mailer.putExtra(Intent.EXTRA_EMAIL, new String[]{MMail});
                    mailer.putExtra(Intent.EXTRA_SUBJECT, "Contacto");
                    mailer.putExtra(Intent.EXTRA_TEXT, "Estimado "+MInmueble);
                    startActivity(Intent.createChooser(mailer, "Enviar correo..."));

                }
            });

            btnCall = (ImageButton) findViewById(R.id.imgBtnLlamarAr);
            btnCall.setOnClickListener(new View.OnClickListener() {
                //@RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + MTelefono));
                    if (ActivityCompat.checkSelfPermission(ListaInmuebleDetalle.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(ListaInmuebleDetalle.this, new String[]{
                                android.Manifest.permission.CALL_PHONE
                        }, 1);

                        return;
                    }
                    startActivity(intent);
                }
            });*/

            new GetContacts().execute();

        }else{
            setContentView(R.layout.f_nulo);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarAr);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
    /*@Override
    public void onBackPressed() {
        if (abrir) {
            Intent intent = new Intent(this, MainActivity.class); startActivity(intent);
        } else {
            super.onBackPressed();
        }
    }*/
    ///VISUALIZAMOS EL BOTON DE BACK
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();// app icon in action bar clicked; go home
                if(abrir){
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent); } ///ABRIMOS MAIN ACTIVITY SI SE ABRIO DESDE ALERTA
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //OBTENEMOS LOS DATOS DEL STRING
    public String runjson(String urlj) throws Exception {
        Request request = new Request.Builder()
                .url(urlj)
                .build();
        if(request != null){
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return response.body().string();
            }else {
                return null;
            }
        }else{
            return null;
        }
    }
    private class GetContacts extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(ListaInmuebleDetalle.this);
            pDialog.setMessage("Cargando...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            //ServiceHandler sh = new ServiceHandler();

            // Making a request to url and getting response
            //String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);
            String jsonStr2 = null;
            try {
                jsonStr2 = runjson(url);
                //Log.v("Text Data-->", jsonStr);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(jsonStr2 != null){
                jsonStrSlide = jsonStr2;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            cargaDetalle(jsonStrSlide);
         }

    }



    public void cargaDetalle(String jsonStr){
        //Log.e("Text Data-->", jsonStr);
        if(jsonStr != null) {
            try {
                jsondata = null;
                JSONObject jsonObj = new JSONObject(jsonStr);
                // Getting JSON Array node
                jsondata = jsonObj.getJSONArray(TAG_TABLA);
                //
                JSONObject c = jsondata.getJSONObject(0); //OBJETO
                //
                /*String JSTitulo = c.getString(TAG_TITULO);
                String JSFoto = c.getString(TAG_FOTOMIN);
                String JSFecha = c.getString(TAG_FECHA);
                String JSLugar = c.getString(TAG_LUGAR);
                String JSTexto = c.getString(TAG_TEXTO);*/


                TextView txtNombre = (TextView) ListaInmuebleDetalle.this.findViewById(R.id.textNombreIn);
                TextView txtDimension = (TextView) ListaInmuebleDetalle.this.findViewById(R.id.textDimensionIn);
                TextView txtUbica = (TextView) ListaInmuebleDetalle.this.findViewById(R.id.textUbicaIn);
                TextView txtGestion = (TextView) ListaInmuebleDetalle.this.findViewById(R.id.textGestionIn);
                TextView txtCantidad = (TextView) ListaInmuebleDetalle.this.findViewById(R.id.textCantidadIn);
                TextView txtCosto = (TextView) ListaInmuebleDetalle.this.findViewById(R.id.textCostoIn);
                //
                CollapsingToolbarLayout collapsingToolbar =
                        (CollapsingToolbarLayout) findViewById(R.id.toolbar_layoutAr);
                collapsingToolbar.setTitle(c.getString(TAG_NOMBRE));
                //
                txtNombre.setText(c.getString(TAG_NOMBRE));
                //txtPublico.setText(c.getString(TAG_PUBLICO));
                txtDimension.setText(Html.fromHtml(c.getString(TAG_DIM)));
                txtUbica.setText(c.getString(TAG_UBICA));
                txtGestion.setText(c.getString(TAG_GESTION));
                txtCantidad.setText(c.getString(TAG_CANTIDAD));
                txtCosto.setText(c.getString(TAG_COSTO));
                //para pasar al activity de mama
                MNombre = c.getString(TAG_NOMBRE);
                MUbica = c.getString(TAG_UBICA);
                MGestion = c.getString(TAG_GESTION);
                MCantiHab = c.getString(TAG_CANTIDAD);
                MCosto = c.getString(TAG_COSTO);
                MInmuebleID = c.getString(TAG_ID);
                MDimension = c.getString(TAG_DIM);

                //Glide.with(ListaInmuebleDetalle.this).load(c.getString(TAG_FOTOMIN)).centerCrop().into(imagen);

/*                if (MNombre.trim().isEmpty() || MNombre.equals("null")) {
                    btnMail.setColorFilter(R.color.colorGris);
                    btnMail.setEnabled(false);
                    btnMail.setClickable(false);
                }else{
                    btnMail.setEnabled(true);
                    btnMail.setClickable(true);
                    btnMail.setColorFilter(R.color.colorPrimary);
                }
                if (MUbica.trim().isEmpty() || MUbica.equals("null")) {
                    btnWeb.setColorFilter(R.color.colorGris);
                    btnWeb.setEnabled(false);
                    btnWeb.setClickable(false);
                }else{
                    btnWeb.setEnabled(true);
                    btnWeb.setClickable(true);
                    btnWeb.setColorFilter(R.color.colorPrimary);
                }
                if (MGestion.trim().isEmpty() || MGestion.equals("null")) {
                    btnCall.setColorFilter(R.color.colorGris);
                    btnCall.setEnabled(false);
                    btnCall.setClickable(false);
                }else{
                    btnCall.setEnabled(true);
                    btnCall.setClickable(true);
                    btnCall.setColorFilter(R.color.colorPrimary);
                }
                if (MDimension.trim().isEmpty() || MDimension.equals("0")) {
                    btnMat.setColorFilter(R.color.colorGris);
                    btnMat.setEnabled(false);
                    btnMat.setClickable(false);
                }else{
                    btnMat.setEnabled(true);
                    btnMat.setClickable(true);
                    btnMat.setColorFilter(R.color.colorPrimary);
                }*/
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(this, "Lo sentimos no se pueden cargar los datos.", Toast.LENGTH_SHORT).show();
        }
    }
    public boolean chkInternet(){
        if(checkInternetConnection(ListaInmuebleDetalle.this)) {
            return true;
        }else {
            ///
            Snackbar snackbar = Snackbar
                    .make(getWindow().getDecorView().getRootView(), "No hay conexion a internet", Snackbar.LENGTH_LONG)
                    .setAction("REINTENTAR", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new GetContacts().execute();
                        }
                    });
            // Changing message text color
            snackbar.setActionTextColor(Color.YELLOW);
            // Changing action button text color
            View sbView = snackbar.getView();
            sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        }
        return false;
    }
    public static boolean checkInternetConnection(Context context)
    {
        try{
            ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isAvailable() && conMgr.getActiveNetworkInfo().isConnected())
                return true;
            else
                return false;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
